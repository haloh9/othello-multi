/**
** Server Config
**/
var express = require('express');
var app = express();
var game = require('./game');
var randomstring = require("randomstring"); //https://github.com/klughammer/node-randomstring
var httpServer = require('http').createServer(app);
var io = require('socket.io').listen(httpServer);
httpServer.listen(3030);
console.log('server running at http://localhost:3030/');

app.set('view engine', 'ejs'); // Moteur de template
app.use('/assets', express.static(('public')));
app.get('/', function (req, res){
  res.render('pages/index');
});

/**
** Game Data
**/
var users = {}; // list des users connectées au serveur
var rooms = {}; // list des rooms(Parties) du serveur

/**
** Fonction pour vérifier si un joueur appartien déjà a une room
**/
var has_a_room = function(socket_id, rooms){
  for(i=0; i<rooms.length; i++){
    if(io.sockets.adapter.sids[socket_id][rooms[i]])
      return true;
  }
  return false;
}

/**
** Les Sockets
**/
io.sockets.on('connection', function(socket){

  socket.on('new_login', function(data) {
    if(data.username in users){
      socket.emit('new_login', {error: true});
      return;
    }
    console.log('new_login : '+data.username+' connecté.')
    socket.username = data.username;
    users[socket.username] = socket;
    socket.emit('new_login', {
      username: socket.username,
      error: false
    });
    socket.emit('update_list_rooms', rooms);
    io.sockets.emit('update_users_online', Object.keys(users));
  });

  socket.on('new_room', function() {
    if(has_a_room(socket.id, rooms)) return;
    var room = randomstring.generate(5);
    socket.join(room);
    console.log('new_room : '+room+' créer par '+socket.username);
    socket.logged_room = room;
    rooms[room] = {
      board: game.init_board(),
      name_p1: socket.username,
      name_p2: null,
      is_full: 0
    };
    io.sockets.emit('update_list_rooms', rooms);
    io.to(socket.id).emit('add_navbar_message', {username: socket.username, roomname: socket.logged_room});
    io.to(socket.id).emit('go_to_room', {room_info:rooms[room]}); // on envoie l'information a tous les client de la room
    io.in(room).emit('refresh_board', {board: rooms[room].board});
    io.in(room).emit('server_messages', {
      user: "Système",
      text: "En attente d\'un deuxième joueurs."
    });

  });

  socket.on('sent_msg', function(data) {
    if(socket.username != undefined){
      data.user = socket.username;
      console.log('Message : "'+data.text+'"');
      console.log('Sent by :  '+data.user);
      io.in(socket.logged_room).emit('new_msg', data);
    }
  });

  socket.on('join_room', function(room) {
    if(has_a_room(socket.id, rooms)) return;
    if(rooms[room].is_full) return;
    socket.join(room);
    socket.logged_room = room;
    console.log('join_room : '+socket.username+' join '+room);

    rooms[room].name_p2 = socket.username;
    rooms[room].turn = 'NOT_YET';
    rooms[room].is_full = 1;
    rooms[room].color_p1 = 'black';
    rooms[room].color_p2 = 'white';
    rooms[room].score_p1 = game.get_player_score(rooms[room].board, rooms[room].color_p1);
    rooms[room].score_p2 = game.get_player_score(rooms[room].board, rooms[room].color_p2);
    rooms[room].global_score_p1 = 0;
    rooms[room].global_score_p2 = 0;

    var player1 = rooms[room].name_p1;
    io.sockets.emit('update_list_rooms', rooms);
    io.to(socket.id).emit('go_to_room', {room_info:rooms[room]});
    io.to(socket.id).emit('add_navbar_message', {username: socket.username, roomname: socket.logged_room});
    io.in(room).emit('refresh_info_view', {room_info: rooms[room]});
    io.to(users[player1].id).emit('refresh_hote_view', {text_button: 'Start Game'});
    io.in(room).emit('refresh_board', {board: rooms[room].board});

    io.in(room).emit('server_messages', {
      user: "Système",
      text: "Les deux joueurs sont là, le hote peut lancer la partie."
    });
  });

  socket.on('leave_room', function(room) {
    if(rooms[room].is_full){
      rooms[room].board = game.init_board();
      rooms[room].score_p1 = game.get_player_score(rooms[room].board, rooms[room].color_p1);
      rooms[room].score_p2 = game.get_player_score(rooms[room].board, rooms[room].color_p2);
      rooms[room].global_score_p1 = 0;
      rooms[room].global_score_p2 = 0;
      rooms[room].is_full = 0;
      rooms[room].turn = 'PLAY_OFF';
      if(socket.username == rooms[room].name_p2){
        rooms[room].name_p2 = null;
        var player1 = users[rooms[room].name_p1];
        io.to(player1.id).emit('server_messages', {
          user: "Système",
          text: "Votre adversaire <strong>"+socket.username+"</strong> a quitté la partie<br>Le jeu a été réinitialiser en attendant un autre adversaire."
        });
      }else if(socket.username == rooms[room].name_p1){
        rooms[room].name_p1 = rooms[room].name_p2;
        rooms[room].name_p2 = null;
        var player1 = users[rooms[room].name_p1];
        io.to(player1.id).emit('server_messages', {
          user: "Système",
          text: "Votre adversaire <strong>"+socket.username+"</strong> a quitté la partie<br>Vous êtes le nouvel hôte de la room {"+socket.logged_room+"}.<br>Le jeu a été réinitialiser en attendant un autre adversaire."
        });
      }
      socket.leave(room);
      io.in(room).emit('refresh_board', {board: rooms[room].board});
      io.in(room).emit('refresh_info_view', {room_info: rooms[room]});
    } else {
        console.log('room-deleted :'+room);
        delete rooms[room];
      }
    io.sockets.emit('update_list_rooms', rooms);
    io.to(socket.id).emit('go_to_list_room');
  });

  socket.on('start_game', function() {
    var room = socket.logged_room;
    var player1 = users[rooms[room].name_p1];
    var player2 = users[rooms[room].name_p2];
    rooms[room].board = game.init_board();
    rooms[room].score_p1 = game.get_player_score(rooms[room].board, rooms[room].color_p1);
    rooms[room].score_p2 = game.get_player_score(rooms[room].board, rooms[room].color_p2);
    rooms[room].turn = 'player1';
    var legal_blocks = game.list_legal_block(rooms[room].board, rooms[room].color_p1);
    console.log('game_starting in : '+room+' => '+player1.username+' VS '+player2.username);
    io.in(room).emit('game_starting', rooms[room]);
    io.in(room).emit('refresh_board', {board: rooms[room].board});
    io.to(player1.id).emit('refresh_legal_blocks', {legal_blocks: legal_blocks});
    io.in(room).emit('server_messages', {
      user: "Système",
      text: "Le hote à lancer la partie."
    });
  });

  socket.on('next_player', function (data) {
    var winner;
    // On récupére les informations a propos de la partie (room)
    var room = socket.logged_room;
    var player1 = users[rooms[room].name_p1];
    var player2 = users[rooms[room].name_p2];

    if(socket.username == rooms[room].name_p1 && rooms[room].turn == 'player1'){
      if(game.is_legal(data.row, data.col, rooms[room].color_p1, rooms[room].board, 'noapply') != false){
        // Si la case ou le joueur veut jouer est légal, on là joue puis on met à jour le plateau
        game.is_legal(data.row, data.col, rooms[room].color_p1, rooms[room].board, 'apply');
        // On récupére le nouveau score des deux joueurs
        rooms[room].score_p1 = game.get_player_score(rooms[room].board, rooms[room].color_p1);
        rooms[room].score_p2 = game.get_player_score(rooms[room].board, rooms[room].color_p2);
        legal_blocks_p1 = game.list_legal_block(rooms[room].board, rooms[room].color_p1);
        legal_blocks_p2 = game.list_legal_block(rooms[room].board, rooms[room].color_p2);
        /**
        ** On vérifie si le player2 peut jouer son tour
        **  => Si c'est le cas, on lui accorde le prochain tour
        **  => Sinon, on vérifie si le player1 peut rejouer
        **  => Si le player1 peut rejouer, on lui accorde le prochain tour
        **  => Sinon, tant que les deux joueurs ne peuvent pas jouer, on déclare la fin de la partie
        **/
        if(legal_blocks_p2.length > 0){
          rooms[room].turn = 'player2';
          io.in(room).emit('refresh_board', {board: rooms[room].board});
          io.to(player2.id).emit('refresh_legal_blocks', {legal_blocks: legal_blocks_p2});
          io.in(room).emit('refresh_info_view', {room_info: rooms[room]});
        } else if (legal_blocks_p1.length > 0){
          rooms[room].turn = 'player1';
          io.in(room).emit('refresh_board', {board: rooms[room].board});
          io.to(player1.id).emit('refresh_legal_blocks', {legal_blocks: legal_blocks_p1});
          io.in(room).emit('refresh_info_view', {room_info: rooms[room]});
        } else {
          rooms[room].turn = 'GAME_OVER';
          io.in(room).emit('refresh_board', {board: rooms[room].board});
          // vérifiié qul joueur a gagné
          if(rooms[room].score_p1>rooms[room].score_p2){
            rooms[room].global_score_p1++;
            winner = player1.username;
          } else if (rooms[room].score_p1<rooms[room].score_p2){
            rooms[room].global_score_p2++;
            winner = player2.username;
          }
          io.in(room).emit('game_over', {winner: winner});
          io.in(room).emit('refresh_info_view', {room_info: rooms[room]});
          io.to(player1.id).emit('refresh_hote_view', {text_button: 'Play Again'});
          io.in(room).emit('server_messages', {
            user: "Système",
            text: "Partie terminée.<br>Le hote <strong>"+player1.username+"</strong> peut relancer une nouvelle partie."
          });
        }

      } else {
        io.to(socket.id).emit('server_messages', {
          user: "Système",
          text: "Vous ne pouvez pas jouer dans cette case."
        });
      }

    } else if (socket.username == rooms[room].name_p2 && rooms[room].turn == 'player2'){
        if(game.is_legal(data.row, data.col, rooms[room].color_p2, rooms[room].board, 'noapply') != false){
          game.is_legal(data.row, data.col, rooms[room].color_p2, rooms[room].board, 'apply');
          rooms[room].score_p1 = game.get_player_score(rooms[room].board, rooms[room].color_p1);
          rooms[room].score_p2 = game.get_player_score(rooms[room].board, rooms[room].color_p2);
          legal_blocks_p1 = game.list_legal_block(rooms[room].board, rooms[room].color_p1);
          legal_blocks_p2 = game.list_legal_block(rooms[room].board, rooms[room].color_p2);

          if(legal_blocks_p1.length > 0){
            rooms[room].turn = 'player1';
            io.in(room).emit('refresh_board', {board: rooms[room].board});
            io.to(player1.id).emit('refresh_legal_blocks', {legal_blocks: legal_blocks_p1});
            io.in(room).emit('refresh_info_view', {room_info: rooms[room]});
          } else if (legal_blocks_p2.length > 0){
            rooms[room].turn = 'player2';
            io.in(room).emit('refresh_board', {board: rooms[room].board});
            io.to(player2.id).emit('refresh_legal_blocks', {legal_blocks: legal_blocks_p2});
            io.in(room).emit('refresh_info_view', {room_info: rooms[room]});
          } else {
            rooms[room].turn = 'GAME_OVER';
            io.in(room).emit('refresh_board', {board: rooms[room].board});
            // vérifiié qul joueur a gagné
            if(rooms[room].score_p1>rooms[room].score_p2){
              rooms[room].global_score_p1++;
              winner = player1.username;
            } else if (rooms[room].score_p1<rooms[room].score_p2){
              rooms[room].global_score_p2++;
              winner = player2.username;
            }
            io.in(room).emit('game_over', {winner: winner});
            io.in(room).emit('refresh_info_view', {room_info: rooms[room]});
            io.to(player1.id).emit('refresh_hote_view', {text_button: 'Play Again'});
            io.in(room).emit('server_messages', {
              user: "Système",
              text: "Partie terminée.<br>Le hote <strong>"+player1.username+"</strong> peut relancer une nouvelle partie."
            });
          }

        } else {
          io.to(socket.id).emit('server_messages', {
            user: "Système",
            text: "Vous ne pouvez pas jouer dans cette case."
          });
        }

    } else if (rooms[room].turn == 'PLAY_OFF') {
      io.to(socket.id).emit('server_messages', {
        user: "Système",
        text: "Adversaire non trouvé."
      });
    } else if (rooms[room].turn == 'NOT_YET'){
      io.to(socket.id).emit('server_messages', {
        user: "Système",
        text: "Le hote n\'a pas encore lancer la partie."
      });
    } else if (rooms[room].turn == "GAME_OVER") {
      // en cours....
    } else {
      io.to(socket.id).emit('server_messages', {
        user: "Système",
        text: "Patientez, votrez adversaire n\'a pas encore jouer son tour."
      });
    }

  });

  socket.on('disconnect', function(data) {
    if(!socket.username) return; // si c'était un simple visiteur(non connecter)
    if(socket.logged_room){
      var room = socket.logged_room;
      if(rooms[room].is_full){
        rooms[room].board = game.init_board();
        rooms[room].score_p1 = game.get_player_score(rooms[room].board, rooms[room].color_p1);
        rooms[room].score_p2 = game.get_player_score(rooms[room].board, rooms[room].color_p2);
        rooms[room].global_score_p1 = 0;
        rooms[room].global_score_p2 = 0;
        rooms[room].is_full = 0;
        rooms[room].turn = 'PLAY_OFF';
        if(socket.username == rooms[room].name_p2){
          rooms[room].name_p2 = null;
          var player1 = users[rooms[room].name_p1];
          io.to(player1.id).emit('server_messages', {
            user: "Système",
            text: "Votre adversaire <strong>"+socket.username+"</strong> a quitté la partie<br>Le jeu a été réinitialiser en attendant un autre adversaire."
          });
        }else if(socket.username == rooms[room].name_p1){
          rooms[room].name_p1 = rooms[room].name_p2;
          rooms[room].name_p2 = null;
          var player1 = users[rooms[room].name_p1];
          io.to(player1.id).emit('server_messages', {
            user: "Système",
            text: "Votre adversaire <strong>"+socket.username+"</strong> a quitté la partie<br>Vous êtes le nouvel hôte de la room {"+socket.logged_room+"}.<br>Le jeu a été réinitialiser en attendant un autre adversaire."
          });
        }
        socket.leave(room);
        io.in(room).emit('refresh_board', {board: rooms[room].board});
        io.in(room).emit('refresh_info_view', {room_info: rooms[room]});
      } else {
          console.log('room-deleted :'+room);
          delete rooms[room];
        }
    }
    console.log('disconnect : '+socket.username+' déconnecter.')
    delete users[socket.username];
    io.sockets.emit('update_users_online', Object.keys(users));
    io.sockets.emit('update_list_rooms', rooms);
  });
});
