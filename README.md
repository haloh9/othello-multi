# Othello
## Le jeu
#### Déscription
Othello est un jeu de table qui oppose deux joueurs sur un plateau de 64 cases (8*8), avec des pions bicolores, noirs d'un côté et blancs de l'autre.
Le but du jeu est d'avoir plus de pions de sa couleur que l'adversaire à la fin de la partie, celle-ci se termine lorsque aucun des deux joueurs ne peut plus jouer de coup dite légal, et ça arrive généralement lorsque les 64 cases du plateau sont occupées.

Au début de la partie, le plateau est initialisé de façon à ce que 2 pions de chaque joueur soit posé sur les quatres cases centrales du plateau, et que chaque paire de pions de la même couleur forme une diagonale.
C'est le joueur disposant des pions de couleur noir qui commencera le jeu à chaque début de partie.
Les joueurs jouent chacun leurs tour en posant un pion de leur couleur sur une case libre, de telle façon qu'entre ce pion posé et un autre pion de la même couleur,  au moins un pion de la couleur adverse soit pris en tenaille, selon une ligne horizontale, verticale ou diagonale.
Il retourne alors de sa couleur le “ou” les pions qu'il vient d'encadrer.



#### Fonctionnalités implémentés
Pour le développement de cette application j'ai créé un fichier server.js qui va contenir la logique de notre serveur, ainsi qu'un dossier "public" contenant le fichier style.css, les images et le client.js.
puis le dossier "Views" qui va contenir nos vus sous forme de fichier .ejs

**Connexion d'un joueur**
> Un joueur est obligé de s'identifier pour qu'il puisse créer/rejoindre une partie du jeu, donc lorsqu'il s'identifie, son nom ainsi que sa socket sera sauvegardée dans le tableau des utilisateurs connectées à fin de faciliter la communication ultérieurement.

**Création d'une partie**
> l'application vérifie d'abord si l'utilisateur qui souhaite créer la partie n'appartient pas déjà à une autre partie pour ensuite la créer en lui donnant un nom aléatoire puis en mettant en place les données relatives à cette partie avant de les envoyer au client pour les affichers.
Le jeu ne commencera que lorsque un deuxième joueur rejoint la partie.

**Rejoindre une partie**
> Lorsqu'un joueur rejoint une partie, on remet à jour les informations de la partie avec les données du nouveau joueur et on demande à l'hôte de la partie de lancer le jeu.

**Commencer une partie**
> Lorsque l'hôte lance la partie, ça sera à lui de jouer en premier, alors le serveur demandera au client d'afficher sur le canvas la liste des cases où il a la possibilité de jouer,il joue donc son tour, puis il passe le tour au joueur numéro 2, ainsi de suite, jusqu'à la fin de la partie.

**Fin d'une partie**
> Au tour de chaque joueur, on vérifie si il existe des cases dans le plateau sur lesquelles il peut poser son pion, si le joueur ne peut plus poser de pions sur le plateau, il passe son tour au joueur suivant, si lui aussi ne peut plus poser de pions, on déclare la fin de la partie, et le gagnant sera celui qui a le plus de pion de sa couleur sur le plateau.
on met donc à jour le score global des parties gagné entre les deux joueurs, et on permet à l'hôte de la partie de relancer une nouvelle partie s'il le souhaite.

**Messagerie instantané**
> J'ai mis en place un système de messagerie instantané qui permet aux deux joueurs d'une certaine partie de communiquer entre eux ainsi que de recevoir des messages d'informations de la part du serveur.
Seuls les deux joueurs de la partie peuvent lire/envoyer un message.
et les messages reçu par le système sont des fois personnels et d'autres fois global.

**Quitter une partie**
> Un joueur peut choisir de quitter une partie en plein jeu. donc j'ai mis en place un système qui réinitialise les données relatives à une partie si l'un des deux joueurs quitte cette partie.
Si le joueur qui vient de quitter la partie est l'hôte, le joueur restant deviendra le nouvel hôte de la partie.    
Dans le cas ou les deux joueurs quittent la partie, celle-ci sera supprimé.

**Déconnexion d'un joueur**
> Si le joueur qui souhaite se déconnecter (fermer la page/ le navigateur) appartient à une partie, on le fait quitter la partie comme expliqué précédemment, puis on supprime ses informations du tableau des utilisateurs connectées.
Sinon on supprime directement ses informations du tableau des utilisateurs connectées.

#### Fonctionnalités que j'aurai aimé implémentés
- un système de visionnage de partie en cours, pour permettre aux utilisateurs du site de regarder les parties en cours des autres joueurs en mode spectateur.
- améliorer le système de création d'une partie (room) en permettant à l'utilisateur de choisir un nom à sa partie, un mot de passe s'il le souhaite, et le fait d'activer ou pas le mode spectateur.
- ajouter un timer afin de passer le tour d'un joueur automatiquement s'il dépasse un certain temps d'inactivité.
- mettre en place un profil utilisateur complet.
- stocker toutes les informations dans une base de données.


## Le programme
#### Comment lancer le programme ?

Ouvrir le terminal depuis le dossier contenant le fichier server.js (/othello)
verifier les dépendances :
```bash
npm update
```
puis lancer le serveur :
```bash
node server.js
```
le site sera disponible à partir du lien :
```
http://localhost:3030/
```

#### Liste des packages utilisés

Package     | Description d'utilité
--------    | ---
game        | c'est un module  que j'ai fait et que j'appelle dans server.js, il rassemble les fonctions qui opèrent sur le tableau contenant les informations relative au plateau du jeu comme (l'initialisation du tableau, la récupération et la modification de l'état d'une des cases du tableau, la vérification d'un mouvement si il est légal ou pas ..) afin de donner une meilleur visiblité et une bonne organisation de notre code.
express | C'est le framework utilisé pour le développement de cette application qui est basé sur NodeJs, il nous permet gérer les éléments accessible depuis le navigateur et d'utiliser un moteur de template pour les vues.
ejs         | Afin de mieux organiser mes codes html (views), j'ai utilisé la solution proposé par Express qui est le système de template qui m'a permis d'installer le langage de template Embedded JavaScript (EJS) connus par sa simplicité.
randomstring|  Nous permets de générer un string alphanumeric de 5 caractères dans notre exemple, pour ensuite le donner comme nom pour une room (partie) à la création de celle-ci.

#### Morceau de code source trouvé ailleurs
```
io.sockets.adapter.sids[socket_id][rooms[i]]
```

- Provenance du code : http://stackoverflow.com/a/35060708
- explication du code : retourne vrais si l'utilisateur fait parti de la socket-room 'i', undefined sinon.

####  Répartition client / serveur
Dans ce programme, toutes les données sont stockés et traité côté serveur puis envoyé au client pour être affiché.
donc le client s'occupe par exemple de:
- dessiner le plateau de départ et le mettre à jour à la demande du serveur grâce aux données relatives au plateau du jeu qui sont stockées et envoyées par le serveur.
- mettre à jour la liste des utilisateurs connectées et des parties (rooms) disponible à la demande du serveur.

et le serveur s'occupe de :
- manipuler le tableau contenant la liste des utilisateurs connectées en appliquant les opérations (d'insertion / suppression ) lors d'une nouvelle connexion ou d'une déconnexion d'un joueur.
- traiter toutes les informations à propos d'une room tel que (Nom des joueurs, etat du jeu, les scores..) et émet au client pour lui demander d'afficher ces informations d'une façon organisé.

le client s'occupe aussi de récupérer les coordonnées de la case sur laquelle un joueur tente de poser son pion pour ensuite l'envoyer au serveur qui vérifiera si le mouvement que le joueur essaye de faire est légal ou pas, puis il effecteura des modifications sur le tableau contenant les information relative au plateau du jeu, pour enfin l'envoyer au client à fin qu'il met à jour le plateau visuelle.
