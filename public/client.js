var socket = io.connect();
/**
** Les constantes
**/
var NBR_OF_ROWS = 8;
var NBR_OF_COLS = 8;
var BLOCK_COLOR_1 = 'rgba(33, 156, 39, 0.8)';
var	BLOCK_COLOR_2 = 'rgba(33, 98, 39, 0.8)';
var PION_WHITE = 'white';
var PION_BLACK = 'black';
var user_name;

function draw_board(){
  canvas = document.getElementById('board');
  ctx = canvas.getContext('2d');
  ctx.clearRect(0, 0, canvas.width, canvas.height);
  BLOCK_SIZE = canvas.height / NBR_OF_ROWS; // taille d'une case du board
  var id_case; // utile pour la coloration par default des cases du board
  for(i=0; i<NBR_OF_ROWS; i++){
    id_case = 1+i;
    //Dessiner les 8 colonnes de i=0 à i=7
    for(j=0; j<NBR_OF_COLS; j++){
      //Dessiner le 8 cases pour chaque colonnes
      if(id_case%2 == 0)
        ctx.fillStyle = BLOCK_COLOR_1;
      else
        ctx.fillStyle = BLOCK_COLOR_2;
      ctx.beginPath();
      ctx.fillRect(i*BLOCK_SIZE,j*BLOCK_SIZE,BLOCK_SIZE, BLOCK_SIZE);
      id_case++;
    }
  }
}

function board_click(ev){
  /**
  ** Help : http://www.html5canvastutorials.com/advanced/html5-canvas-mouse-coordinates/
  **/
  var rect = document.getElementById('board').getBoundingClientRect();
  var row = Math.trunc((ev.clientY-rect.top)/BLOCK_SIZE); // récupérer le numéro (ligne) de la case cliqué
  var col = Math.trunc((ev.clientX-rect.left)/BLOCK_SIZE); // recupérer le numéro (colonne) de la case cliqué
  console.log(ev.clientY+" "+row+" => "+ev.clientX+" "+col);
  socket.emit("next_player", {
    row : row,
    col : col
  });
}

var submit_login = document.getElementById('submit_login');
var submit_new_room = document.getElementById('submit_new_room');
var left_box_tile = document.getElementById('left_box_tile');
var right_box_tile = document.getElementById('right_box_tile');
var join_button = document.getElementsByClassName('join_button');
var last_main_box;

submit_login.addEventListener("click",function(e){
  e.preventDefault();
  if(username.value.length <= 4 || username.value.length >=15){
    error_login.innerHTML = "<code>Votre pseudo est trop petit ou trop grand.</code>";
    username.focus();
    return;
  }
  data = {username:username.value};
  socket.emit('new_login', data);
},false);

submit_new_room.addEventListener("click",function(e){
  e.preventDefault();
  socket.emit('new_room');
},false);

submit_msg.addEventListener("click",function(e){
  e.preventDefault();
  var input_msg =  document.getElementById("input_msg");
  if(input_msg.value.length > 0 && input_msg.value.length < 200){
    socket.emit("sent_msg", {
      text : input_msg.value
    });
    document.getElementById("input_msg").value = "";
  }else if(input_msg.value.length > 200)
    alert("La taille maximal d'un message est de 200 caractères !")
  document.getElementById('input_msg').focus();
},false);

socket.on('new_login', function(data) {
  if(data.error){
    document.getElementById('error_login').innerHTML = "<code>ce pseudo est déjà existant, veuillez en choisir un autre</code>";
    username.value = "";
    username.focus();
    return;
  }
  this.user_name = data.username;
  document.getElementById('login_box').remove();
  document.getElementById('main_box').style.display = "";
  document.getElementById('welcome_msg').innerHTML = "Bonjour <strong>"+data.username+"</strong>, vous pouvez soit créer, soit rejoindre une partie.";
});

socket.on('update_users_online', function(data) {
  var users_online = document.getElementById('users_online');
  users_online.innerHTML = "";
  for(i=0; i<data.length; i++){
    var user_li = document.createElement('li');
    user_li.appendChild(document.createTextNode(data[i]));
    users_online.appendChild(user_li);
  }
});

socket.on('update_list_rooms', function(data) {
  // Supprime la liste actuelle
  var list_rooms = document.getElementById('list_rooms');
  var tableHeaderRowCount = 1;
  var rowCount = list_rooms.rows.length;
  for (var i = tableHeaderRowCount; i < rowCount; i++)
    list_rooms.deleteRow(tableHeaderRowCount);
  // Rajoute la nouvelle liste
  for(var room in data){
    var ligne = list_rooms.insertRow(-1);//on a ajouté une lign
    var colonne1 = ligne.insertCell(0);
    colonne1.innerHTML = room;
    var colonne2 = ligne.insertCell(1);
    colonne2.innerHTML = data[room].name_p1;
    var colonne3 = ligne.insertCell(2);
    var colonne4 = ligne.insertCell(3);

    if(data[room].is_full){
      colonne3.innerHTML = '<span class="tag is-warning">complet</span>'
      colonne4.innerHTML = '<a class="button is-primary" onclick="watch_room(\''+room+'\')" ><span class="icon is-small"><i class="fa fa-eye"></i></span><span>Watch</span></a>';
    } else {
      colonne3.innerHTML = '<span class="tag is-success">1/2</span>'
      colonne4.innerHTML = '<a class="button is-primary" onclick="join_room(\''+room+'\')" ><span class="icon is-small"><i class="fa fa-play-circle"></i></span><span>Join</span></a>';
    }
  }
});

function join_room(room){
  socket.emit('join_room', room);
}

function watch_room(room){
alert("Ce système n'est pas encore fonctionnelle");
}


function leave_room(room){
  socket.emit('leave_room', room);
}

socket.on('go_to_room', function(data) {
  // Affiche la room(game_box) et cache la main_box
  document.getElementById('main_box').style.display = "none";
  document.getElementById('game_box').style.display = "";

  // Clear chatbox_messages.
  document.getElementById('all_messages').innerHTML = "";
  // On donnes les info au tableau d'informations
  document.getElementById('game_player1').innerHTML = data['room_info'].name_p1;
  document.getElementById('game_msg_info').innerHTML = 'En attente d\'un adversaire...';
  if(data.name_p2!=null){
    document.getElementById('game_player2').innerHTML = data['room_info'].name_p2;
    document.getElementById('game_msg_info').innerHTML = 'Les deux joueurs sont là<br>Le hote doit lancer la partie';
  }

  draw_board(); // dessiner le plateau de jeu vide.
  document.body.scrollTop = document.body.scrollHeight; // un pti scrool pour un full affichage du plateau

});

socket.on('add_navbar_message', function(data) {
    // On donnes les infos au navbar_game
    document.getElementById('navbar_msg_game').innerHTML = 'Bonjour &nbsp; <b>'+data["username"]+'</b>, vous êtes connecté à la room {'+data['roomname']+'}'
    document.getElementById('navbar_right_side').innerHTML = '<a class="button is-danger" onclick="leave_room(\''+data['roomname']+'\')"><span class="icon"><i class="fa fa-chevron-circle-left"></i></span><span>Quitter la room</span></a>';
});

socket.on('go_to_list_room', function() {
  document.getElementById('main_box').style.display = "";
  document.getElementById('game_box').style.display = "none";
});

socket.on('new_msg', function(data){
  document.getElementById('all_messages').innerHTML += '<div class="server_messages"><code style="color:black;"><strong style="color:blue;">'+data.user+': </strong>'+data.text+'</code></div>';
  document.getElementById('all_messages').scrollTop = all_messages.scrollHeight;
});

socket.on('game_starting', function(data) {
  var user;
  if(data.turn == 'player1')
    user = data.name_p1;
  else
    user = data.name_p2;
  document.getElementById('game_msg_info').innerHTML = 'Le joueur <strong>'+user+'</strong> commence.';
  document.getElementById('game_score_player1').innerHTML = data.score_p1;
  document.getElementById('game_score_player2').innerHTML = data.score_p2;
  canvas.addEventListener('click', board_click, false);
});

socket.on('refresh_hote_view', function(data) {
  document.getElementById('game_msg_info').innerHTML = '<a class="button is-success is-outlined" id="start_game_button">'+data.text_button+'</a>';
  document.getElementById('start_game_button').addEventListener("click",function(e){
    e.preventDefault();
    socket.emit('start_game');
  },false);
});

socket.on('refresh_info_view', function(data) {
  document.getElementById('game_player1').innerHTML = data['room_info'].name_p1+'<strong>('+data['room_info'].global_score_p1+')</strong>';

  if(data['room_info'].name_p2!=null)
    document.getElementById('game_player2').innerHTML = data['room_info'].name_p2 +'<strong>('+data['room_info'].global_score_p2+')</strong>';
  else
    document.getElementById('game_player2').innerHTML = "Null";

  document.getElementById('game_score_player1').innerHTML = data['room_info'].score_p1;
  document.getElementById('game_score_player2').innerHTML = data['room_info'].score_p2;

  if(data['room_info'].turn == 'player1')
    document.getElementById('game_msg_info').innerHTML = 'C\'est le tour de : <strong>'+data['room_info'].name_p1+'</strong> ('+data['room_info'].color_p1+')';
  else if(data['room_info'].turn == 'player2')
    document.getElementById('game_msg_info').innerHTML = 'C\'est le tour de : <strong>'+data['room_info'].name_p2+'</strong> ('+data['room_info'].color_p2+')';
  else if(data['room_info'].turn == 'PLAY_OFF')
    document.getElementById('game_msg_info').innerHTML = 'la partie a été interrompue.';
  else if(data['room_info'].turn == "NOT_YET")
    document.getElementById('game_msg_info').innerHTML = 'Le hote doit lancer la partie.';
  else if(data['room_info'].turn == "GAME_OVER"){
    document.getElementById('game_msg_info').innerHTML = 'La partie est terminée<br>';

  }


});

socket.on('refresh_board', function(data) {
  draw_board();
  var board = data.board;
  for(i=0; i<board.length; i++){
    switch (board[i].state) {
      case "black":
        ctx.beginPath();
        ctx.arc((board[i].col*BLOCK_SIZE)+BLOCK_SIZE/2, (board[i].row*BLOCK_SIZE)+BLOCK_SIZE/2, BLOCK_SIZE/3, 0, 2 * Math.PI, false);
        ctx.fillStyle = PION_BLACK;
        ctx.fill();
        ctx.lineWidth = 2;
        ctx.strokeStyle = '#003300';
        ctx.stroke();
        break;
      case "white":
        ctx.beginPath();
        ctx.arc((board[i].col*BLOCK_SIZE)+BLOCK_SIZE/2, (board[i].row*BLOCK_SIZE)+BLOCK_SIZE/2, BLOCK_SIZE/3, 0, 2 * Math.PI, false);
        ctx.fillStyle = PION_WHITE;
        ctx.fill();
        ctx.lineWidth = 2;
        ctx.strokeStyle = '#003300';
        ctx.stroke();
        break;
    }
  }
  document.body.scrollTop = document.body.scrollHeight; // un pti scrool pour un full affichage du plateau
});

socket.on('game_over', function(data) {

  var x =  50;
  var y = canvas.height / 3;
  var winner = data.winner;

  ctx.beginPath();
  ctx.rect(x, y, canvas.width-(x*2), 150);
  ctx.fillStyle = '#FFFFFF';
  ctx.fillStyle = 'rgba(225,225,225,0.9)';
  ctx.fill();
  ctx.lineWidth = 2;
  ctx.strokeStyle = '#000000';
  ctx.stroke();
  ctx.closePath();
  ctx.font = '20pt Sans-Serif';
  ctx.fillStyle = 'rgb(204, 111, 43)';
  ctx.fillText('Partie terminée', x+20, y+40);
  if(winner == 'nul'){
    ctx.fillStyle = 'rgb(204, 111, 43)';
    ctx.font = 'Bold 20pt Sans-Serif';
    ctx.fillText('Les deux joueurs', x+60, y+80);
    ctx.font = '20pt Sans-Serif';
    ctx.fillText('sont à égalité', x+100, y+120);
  } else {
    ctx.fillStyle = 'rgb(204, 111, 43)';
    ctx.font = 'Bold 20pt Sans-Serif';
    ctx.fillText('Le joueur \''+winner+'\'', x+60, y+80);
    ctx.font = '20pt Sans-Serif';
    ctx.fillText('remporte la partie', x+100, y+120);
  }


  document.body.scrollTop = document.body.scrollHeight; // un petit scrool pour un full affichage du plateau
});

socket.on('refresh_legal_blocks', function(data) {
  for(var block of data.legal_blocks){
    ctx.beginPath();
    ctx.arc((block.col*BLOCK_SIZE)+BLOCK_SIZE/2, (block.row*BLOCK_SIZE)+BLOCK_SIZE/2, BLOCK_SIZE/3, 0, 2 * Math.PI, false);
    ctx.fillStyle = "rgba(215, 81, 13, 0.6)";
    ctx.fill();
    ctx.lineWidth = 2;
    ctx.strokeStyle = '#003300';
    ctx.stroke();
  }
});

socket.on('server_messages', function(data) {
  document.getElementById('all_messages').innerHTML += '<div class="server_messages"><code><strong>système =></strong> '+data.text+'</code></div>';
  document.getElementById('all_messages').scrollTop = all_messages.scrollHeight;
});
