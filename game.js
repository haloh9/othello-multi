var init_board = function(){
  var board = new Array(); // déclaration du tableau
  /**
  ** Initialisation du tableau
  **/
  for(i=0; i<8; i++){
    for(j=0; j<8; j++){
      var block = { row: j, col: i, state: "free" };
      board.push(block);
    }
  }
  change_block_state(board, 3, 4, "black");
  change_block_state(board, 4, 3, "black");
  change_block_state(board, 4, 4, "white");
  change_block_state(board, 3, 3, "white");
  return board;
}

/**
** Change l'état d'une case du plateau par celui donné en paramétre
**/
var change_block_state = function(board, row, col, state){
  for(i=0; i<board.length; i++){
    if(board[i].row == row && board[i].col == col)
      board[i].state = state;
  }
}

/**
** Récupère l'état d'une case du plateau donnée en paramètre
**/
var get_block_state = function(board, row, col){
  for(i=0;i<board.length;i++){
    if(board[i].row == row && board[i].col == col)
      return board[i].state;
  }
}

/**
** Determine si le joueur peut ou ne peut pas poser son pion dans une case.
** Si action = "apply" : la fonction appliquera les changements dans le plateau
** Si action = "noapply" : la fonction retournera true si le joueur peut poser un pion, false sinon.
**/
var is_legal = function(row, col, player, board, action){
  var tmp_row, tmp_col, enemy;
  var legal = false;
  var tab= new Array();
  if(player=="black")
    enemy = "white";
  else
    enemy = "black";
  if(get_block_state(board,row,col) != "free") return false; // SI la case clické n'est pas vide, return false
  for(dx=-1; dx<= 1;dx++){
    for(dy=-1; dy<= 1;dy++){
      if((dx==0)&&(dy==0)) continue;
      iter=1;
      tmp_row = row+iter*dx;
      tmp_col = col+iter*dy;
      if(get_block_state(board,tmp_row, tmp_col) != enemy) continue;
      while(tmp_row>=0 && tmp_row <=7 && tmp_col>=0 && tmp_row <=7){
        tmp_row = row+iter*dx;
        tmp_col = col+iter*dy;
        if(get_block_state(board, tmp_row, tmp_col) == enemy){
          iter++;
          continue;
        }
        if(get_block_state(board,tmp_row, tmp_col) != player) break;
        var info_iter = {
          player: player,
          row: row,
          col: col,
          dx: dx,
          dy: dy,
          iter: iter
        };
        if(action == 'apply')
          apply_change_to_board(board, info_iter);
        legal = true;
        break;
      }

    }
  }
  return legal;
}

/**
** Récupère la liste de toutes les cases où le joueur donnée en paramètre peut jouer.
**/
var list_legal_block = function(board, player){
  var list_lb = new Array(); // déclaration du tableau
  for(var block of board){
    if(is_legal(block.row, block.col, player, board, 'noapply') != false){
      list_lb.push({
        row: block.row,
        col: block.col
      });
    }
  }
  return list_lb;
}

var apply_change_to_board = function(board, info_iter){
  var iter,row, col;
  iter = info_iter.iter;
  while(iter>=0){
    row = info_iter.row + (iter*info_iter.dx);
    col = info_iter.col + (iter*info_iter.dy);
    change_block_state(board, row, col, info_iter.player);
    iter--;
  }

}

/**
** Récupère le score du joueur donné en paramètre.
**/
var get_player_score = function(board, player_color){
  var cpt=0;
  for(var block of board){
    if(block.state == player_color)
      cpt++;
  }
  return cpt;
}

exports.init_board = init_board;
exports.change_block_state = change_block_state;
exports.get_block_state = get_block_state;
exports.is_legal = is_legal;
exports.list_legal_block = list_legal_block;
exports.apply_change_to_board = apply_change_to_board;
exports.get_player_score = get_player_score;
